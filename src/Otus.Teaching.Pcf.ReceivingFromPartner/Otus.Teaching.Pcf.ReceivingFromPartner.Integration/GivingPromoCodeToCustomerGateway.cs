﻿using System.Net.Http;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using MassTransit;
using  MassTransit.RabbitMqTransport;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class GivingPromoCodeToCustomerGateway
        : IGivingPromoCodeToCustomerGateway
    {
        private readonly HttpClient _httpClient;

        public GivingPromoCodeToCustomerGateway(HttpClient httpClient, IPublishEndpoint publishEndpoint)
        {
            _httpClient = httpClient;
        }
        
        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            var dto = new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            };
            
            var response = await _httpClient.PostAsJsonAsync("api/v1/promocodes", dto);

            response.EnsureSuccessStatusCode();
        }
         public async Task GivePromoCodeToCustomerAsync(PromoCode promoCode)
        {
            var dto = new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            };
            var busControl = Bus.Factory.CreateUsingRabbitMq(Configure);
            await busControl.StartAsync();
            try
            {       
                //PUBLISH
                await busControl.Publish(dto);
            }
            finally
            {
                await busControl.StopAsync();
            }
        }
         
        private static void Configure(IRabbitMqBusFactoryConfigurator configurator)
        {
            configurator.Host("cow.rmq2.cloudamqp.com",
                "xvvcjzoi",
                h =>
                {
                    h.Username("xvvcjzoi");
                    h.Password("3zzqgto8t6iqz6EMWhrx3fj8ubnToHJ6");
                });
        }
    }
}