using System;
using MassTransit;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using SharedModel;
using System.Linq;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Consumer
{
public class GivePromoCodesToCustomersWithPreferenceConsumer : IConsumer<GivePromoCodeToCustomerModel>
{
    public  GivePromoCodesToCustomersWithPreferenceConsumer (IRepository<Preference> preferencesRepository, IRepository<Customer> customerRepository, IRepository<PromoCode> promoCodesRepository)
    {
        _preferencesRepository = preferencesRepository;
        _customerRepository = customerRepository;
        _promoCodesRepository = promoCodesRepository;
    }
    private readonly IRepository<Preference> _preferencesRepository;
    private readonly IRepository<Customer> _customerRepository;
    private readonly IRepository<PromoCode> _promoCodesRepository;
    public async Task Consume(ConsumeContext<GivePromoCodeToCustomerModel> context)
    {
        var message = context.Message;
       //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(message.PreferenceId);

            if (preference == null)
            {
               Console.WriteLine("Отсутствует данное предрочтение");
            }
            else{
             var customers = await _customerRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));
            GivePromoCodeRequest request = new GivePromoCodeRequest{
                ServiceInfo = message.ServiceInfo,
                PartnerId = message.PartnerId,
                PromoCodeId = message.PromoCodeId,
                PromoCode  = message.PromoCode,
                PreferenceId  = message.PreferenceId,              
                BeginDate = message.BeginDate,
                EndDate = message.EndDate
            };
            
            PromoCode promoCode = PromoCodeMapper.MapFromModel(request, preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);
            await context.RespondAsync<string>("works ok!");
            }
        }
}
}