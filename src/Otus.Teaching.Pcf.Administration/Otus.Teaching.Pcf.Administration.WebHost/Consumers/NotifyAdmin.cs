using System;
using MassTransit;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using SharedModel;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Extensions;

namespace Otus.Teaching.Pcf.Administration.Core.Consumer
{
    public class NotifyAdminConsumer : IConsumer<PartnerManagerModel>
    {
        public  NotifyAdminConsumer (IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        private readonly IRepository<Employee> _employeeRepository;
        public async Task Consume(ConsumeContext<PartnerManagerModel> context)
        {
            var message = context.Message;
            using (UpdatePromocodeExtension updatePromocode = new  UpdatePromocodeExtension(_employeeRepository) )
            {
                try
                {
                    await updatePromocode.UpdatePromocode(message.partnerManagerId);
                }
                catch
                {
                    await context.RespondAsync<string>("employee doesn't exist!");
                }
                await context.RespondAsync<string>("works ok!");
            }   
        }        
    }
}