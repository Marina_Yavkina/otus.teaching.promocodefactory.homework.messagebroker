using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System.IO;

namespace Otus.Teaching.Pcf.Administration.Core.Extensions{
    public class UpdatePromocodeExtension : IDisposable
    {
        public UpdatePromocodeExtension (IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        private readonly IRepository<Employee> _employeeRepository;


        public async Task UpdatePromocode(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                throw new FileNotFoundException("Employee is not found!");;

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);
        }

        public void Dispose() => Console.WriteLine($"disposed");
    }
}